INSERT INTO blogcategory (name) VALUES ('website');
INSERT INTO blogcategory (name) VALUES ('computer');

INSERT INTO blogtag (blogcategory_id, name) VALUES (1, 'test');
INSERT INTO blogtag (blogcategory_id, name) VALUES (1, 'javascript');
INSERT INTO blogtag (blogcategory_id, name) VALUES (1, 'go');
INSERT INTO blogtag (blogcategory_id, name) VALUES (1, 'postgresql');
INSERT INTO blogtag (blogcategory_id, name) VALUES (2, 'sql');
INSERT INTO blogtag (blogcategory_id, name) VALUES (1, 'docker');

INSERT INTO blogfile (filename) VALUES ('test-for-blog.md');
INSERT INTO blogfile (filename) VALUES ('test-website-blog.md');

INSERT INTO blogtagfile (blogtag_id, blogfile_id) VALUES (1, 1);
INSERT INTO blogtagfile (blogtag_id, blogfile_id) VALUES (2, 1);
