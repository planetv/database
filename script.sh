#!/bin/sh

PROJECT=planetv
PROJECT_CONTAINER=planetv-database-container-1
PROJECT_IMAGE=planetv-database-image
PROJECT_NETWORK=planetv_network

# Build docker image
build_stable_image() {
	docker buildx build . -t ${PROJECT_IMAGE}:stable --build-arg ENVIRONMENT=stable
}
build_dev_image() {
	docker buildx build . -t ${PROJECT_IMAGE}:dev --build-arg ENVIRONMENT=development
}

# Start and stop docker compose
start_stable() {
	docker compose -p ${PROJECT} -f docker-compose-stable.yml up -d
}
start_dev() {
	docker compose -p ${PROJECT} -f docker-compose-dev.yml up -d
}
stop() {
	docker compose -p ${PROJECT} stop
}

# Remove docker container, network and image
remove_container() {
	docker compose -p ${PROJECT} down
}
remove_all_image() {
	docker rmi ${PROJECT_IMAGE}:stable
	docker rmi ${PROJECT_IMAGE}:dev
}
remove_all() {
	remove_container
	remove_network
	remove_all_image
}

# Rebuild steps
rebuild_stable_all() {
    remove_all
    build_stable_image
	start_stable
}
rebuild_dev_all() {
    remove_all
    build_dev_image
	start_dev
}

# Access docker container
access() {
	docker exec -it ${PROJECT_CONTAINER} sh
}
access_database() {
	docker exec -it ${PROJECT_CONTAINER} psql -U postgres
}

# Check log docker container
log_container() {
	docker logs ${PROJECT_CONTAINER}
}

# Print list of script options
print_list() {
	echo "Pass wrong arguments! Here is list of arguments for docker script"
	echo -e "\taccess             : access container"
	echo -e "\taccess-database    : access database container"
	echo -e "\tbuild-stable       : build image stable"
	echo -e "\tbuild-dev          : build image development"
	echo -e "\tlog                : log docker container"
	echo -e "\trebuild-stable-all : rebuild stable docker (remove, build and start)"
	echo -e "\trebuild-dev-all    : rebuild dev docker (remove, build and start)"
	echo -e "\tremove-container   : remove container"
	echo -e "\tremove-all-image   : remove all image"
	echo -e "\tremove-all         : remove all (container, network, image)"
	echo -e "\tstart-stable       : start docker compose stable"
	echo -e "\tstart-dev          : start docker compose development"
	echo -e "\tstop               : stop docker compose"
}

# Main script
if [ $# -eq 1 ]; then
	case "$1" in
		"access" )
			access ;;
		"access-database" )
			access_database ;;
		"build-stable" )
			build_stable_image ;;
		"build-dev" )
			build_dev_image ;;
		"log" )
			log_container ;;
		"rebuild-stable-all" )
			rebuild_stable_all ;;
		"rebuild-dev-all" )
			rebuild_dev_all ;;
		"remove-all" )
			remove_all ;;
		"remove-all-image" )
			remove_all_image ;;
		"remove-container" )
			remove_container ;;
		"start-stable" )
			start_stable ;;
		"start-dev" )
			start_dev ;;
		"stop" )
			stop ;;
		* )
			print_list ;;
	esac
else
	print_list
fi
