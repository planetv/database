FROM postgres:16-alpine

# Install additional tools (e.g., pg_ctl) using apk
RUN apk update && \
    apk add --no-cache postgresql-client

# Define build argument
ARG ENVIRONMENT

# Copy the appropriate setup file based on the environment
COPY src /docker-entrypoint-initdb.d/src/

WORKDIR /app

# Use a script to determine which setup file to copy
COPY copy-setup-file.sh ./
COPY setup-database-stable.sql ./
COPY setup-database-dev.sql ./

# Run the script to copy the correct setup file
RUN chmod +x ./copy-setup-file.sh
RUN ./copy-setup-file.sh ${ENVIRONMENT}
