CREATE DATABASE planetv;

\c planetv;

-- Tạo các bảng trong schema public
\i /docker-entrypoint-initdb.d/src/data_tables.sql
\i /docker-entrypoint-initdb.d/src/insert-test-data.sql